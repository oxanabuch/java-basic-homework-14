package com.company.interfaces;

public class Main {

    public static void main(String[] args) {

        SmartPhone smartPhone = new SmartPhone("Samsung", "Android");
        DialPhone dialPhone = new DialPhone("Panasonic");
        Post post = new Post("Kyiv", "Taras");

        smartPhone.call("1252378498");
        smartPhone.editMail();

        dialPhone.call("124845125");

        post.createMail();

        sendMailAll(smartPhone, post);


    }

    public static void sendMailAll(MailSender... devices) {
        for (MailSender device : devices) {
            device.sendMail();
        }

    }
}
