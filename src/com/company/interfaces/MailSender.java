package com.company.interfaces;

public interface MailSender {

    String createMail();

    void sendMail();

}
