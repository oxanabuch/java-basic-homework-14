package com.company.interfaces;

public class SmartPhone implements Caller, EmailSender, MailSender {

    private String model;
    private String os;


    public SmartPhone(String model, String os) {
        this.model = model;
        this.os = os;
    }

    @Override
    public String editMail() {
        return "You can edit the mail.";
    }

    @Override
    public void call(String contact) {
        System.out.println("Call the contact: " + contact);

    }

    @Override
    public String createMail() {
        return "You are writing the e mail.";
    }

    @Override
    public void sendMail() {
        System.out.println("Your e mail is sending");
    }
}
