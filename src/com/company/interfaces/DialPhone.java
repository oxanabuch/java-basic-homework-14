package com.company.interfaces;

public class DialPhone implements Caller {

    private String model;

    public DialPhone(String model) {
        this.model = model;
    }

    @Override
    public void call(String contact) {
        System.out.println("Pick up the phone and call the contact: " + contact);
    }
}
