package com.company.interfaces;

public class Post implements MailSender {
    String address;
    String name;

    public Post(String address, String name) {
        this.address = address;
        this.name = name;
    }

    @Override
    public String createMail() {
        return "You are writing a letter to " + name;
    }

    @Override
    public void sendMail() {
        System.out.println("Your mail is sending to " + address);

    }
}
