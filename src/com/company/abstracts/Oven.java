package com.company.abstracts;

public class Oven extends Cooker {

    private String product;
    private int initTime;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getInitTime() {
        return initTime;
    }

    public void setInitTime(int initTime) {
        this.initTime = initTime;
    }

    public void initTimer() {
        System.out.println("Select time: " + initTime);
    }

    void printInfo() {
        System.out.println("Info about oven: ");
        System.out.println(name);
        System.out.println(serialNumber);
        System.out.println(getInitTime());
        System.out.println(getProduct());
        System.out.println("---");
    }


    @Override
    void cook() {

        System.out.println("You are baking a: " + product);
    }

}
