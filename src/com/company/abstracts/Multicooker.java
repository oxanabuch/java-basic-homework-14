package com.company.abstracts;

public class Multicooker extends Cooker {
    int programNumber;

    public int getProgramNumber() {
        return programNumber;
    }

    public void setProgramNumber(int programNumber) {
        this.programNumber = programNumber;
    }

    void switchProgram(int programNumber) {
        System.out.println("Select a program");
        System.out.println("---");
    }

    void printInfo() {
        System.out.println("Info about multicooker: ");
        System.out.println(name);
        System.out.println(serialNumber);
        System.out.println(getProgramNumber());
        System.out.println("---");
    }


    @Override
    void cook() {
        System.out.println("You are cooking according to the program: " + programNumber);
        System.out.println("---");
    }

    }

