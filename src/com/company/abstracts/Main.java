package com.company.abstracts;

import com.company.interfaces.MailSender;

public class Main {

    public static void main(String[] args) {

        Mobile mobile = new Mobile();
        System.out.println("----------");
        mobile.name = "noname";
        mobile.serialNumber = "DGF543FRT";
        mobile.setDisplay("5.5 inch");
        mobile.setSimCount(1);
        mobile.setNumber("0670000000");

        mobile.powerOn();
        mobile.printInfo();


        DialPhone dialPhone = new DialPhone();
        System.out.println("----------");
        dialPhone.name = "Panasonic";
        dialPhone.serialNumber = "AF2437UA";
        dialPhone.setNumber("0630000000");
        dialPhone.setHasAnswerphone(true);

        dialPhone.powerOn();
        dialPhone.printInfo();
        dialPhone.call();
        dialPhone.autoAnswer();


        SmartPhone smartPhone = new SmartPhone();
        System.out.println("----------");
        smartPhone.name = "Samsung";
        smartPhone.serialNumber = "AF777UA";
        smartPhone.setDisplay("5.5 inch");
        smartPhone.setSimCount(2);
        smartPhone.setOs("Android");
        smartPhone.setNumber("0500000000");

        smartPhone.powerOn();
        smartPhone.printInfo();
        smartPhone.runApp();
        smartPhone.call();


        Multicooker multicooker = new Multicooker();
        multicooker.name = "Cuckoo";
        multicooker.serialNumber = "CMC-HE1055F";
        multicooker.setProgramNumber(7);

        multicooker.powerOn();
        multicooker.printInfo();
        multicooker.cook();


        Oven oven = new Oven();
        oven.name = "Bosh";
        oven.serialNumber = "FSD23279H";
        oven.setInitTime(2);
        oven.setProduct("fish");

        oven.powerOn();
        oven.printInfo();
        oven.initTimer();
        oven.cook();
        System.out.println("---");


        // option 1 - без varargs

//        powerOff(mobile);
//        System.out.println("---");
//        powerOff(dialPhone);
//        System.out.println("---");
//        powerOff(smartPhone);
//        System.out.println("---");
//        powerOff(multicooker);
//        System.out.println("---");
//        powerOff(oven);
//        System.out.println("---");


        powerOff(mobile, dialPhone, smartPhone, multicooker, oven);

    }

    // option 1  - без varargs
//    public static void powerOff(AbstractDevise abstractDevise) {
//
//        System.out.println(abstractDevise.powerOff());
//    }

    // option 2  - з varargs
    public static void powerOff(AbstractDevise... devices) {
        for (AbstractDevise device : devices) {
            device.powerOff();
        }
    }
}
