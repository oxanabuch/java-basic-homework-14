package com.company.abstracts;

abstract class AbstractDevise {

    String name;
    String serialNumber;


    public abstract void powerOn();


    //  option 1 - без varargs
//    public abstract String powerOff();


    // option 2 - з varargs
    public abstract void powerOff();

}
