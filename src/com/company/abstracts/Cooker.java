package com.company.abstracts;

abstract class Cooker extends AbstractDevise {

    @Override
    public void powerOn() {
        System.out.println("Press the power button - ON.");
        System.out.println("I'm ready. You can cook.");
        System.out.println("---");

    }

    // option 1 - без varargs

//    @Override
//    public String powerOff() {
//        return "The food is cooked. Bon appetit. Goodbye. Press the power button - OFF.";
//
//    }


    // option 2 - з varargs
    @Override
    public void powerOff() {
        System.out.println("The food is cooked. Bon appetit. Goodbye. Press the power button - OFF.");

    }

    abstract void cook();

}
