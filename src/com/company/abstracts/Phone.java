package com.company.abstracts;

import com.company.abstracts.AbstractDevise;

abstract class Phone extends AbstractDevise {

    String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    abstract void call();

}
