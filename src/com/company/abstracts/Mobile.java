package com.company.abstracts;

public class Mobile extends Phone {

    private int simCount;
    private String display;

    public int getSimCount() {
        return simCount;
    }

    public void setSimCount(int simCount) {
        this.simCount = simCount;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    @Override
    public void powerOn() {
        System.out.println("Press the power button. The mobile phone starts up. Hello, user.");
        System.out.println("---");
    }


    void printInfo() {
        System.out.println("Info about  mobile phone: ");
        System.out.println(name);
        System.out.println(serialNumber);
        System.out.println(getSimCount());
        System.out.println(getDisplay());
        System.out.println("---");
    }

    // option 1 - без varargs

//    @Override
//    public String powerOff() {
//        return "Goodbye, user. Press the power button.";
//
//    }

    // option 2 - з varargs
    @Override
    public void powerOff() {
        System.out.println("Goodbye, user. Press the power button.");

    }

    @Override
    void call() {
        System.out.println("Yoa are calling the number: " + number);
        System.out.println("---");
    }
}
