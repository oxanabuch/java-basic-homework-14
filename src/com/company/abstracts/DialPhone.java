package com.company.abstracts;

public class DialPhone extends Phone {

    boolean hasAnswerphone;

    public boolean isHasAnswerphone() {
        return hasAnswerphone;
    }

    public void setHasAnswerphone(boolean hasAnswerphone) {
        this.hasAnswerphone = hasAnswerphone;
    }

    void autoAnswer() {
        if (hasAnswerphone = true) {
            System.out.println("The caller cannot accept your call. Leave a massage after the alarm.");
            System.out.println("---");
        }
    }


    void printInfo() {
        System.out.println("Info about device: ");
        System.out.println(name);
        System.out.println(serialNumber);
    }


    @Override
    public void powerOn() {
        System.out.println("Connect dial phone to the network.");
        System.out.println("---");
    }

    // option 1 - без varargs
//    @Override
//    public String powerOff() {
//        return "Disconnect dial phone from the network.";
//    }

    // option 2 - з varargs
    @Override
    public void powerOff() {
        System.out.println("Disconnect dial phone from the network.");
    }


    @Override
    void call() {
        System.out.println("Pick up the phone, dial the number: " + number + " call.");
        System.out.println("You are calling the number: " + number);
        System.out.println("---");
    }
}
