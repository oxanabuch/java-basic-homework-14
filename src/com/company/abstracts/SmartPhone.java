package com.company.abstracts;

public class SmartPhone extends Mobile {

    String os;

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }


    void runApp() {
        System.out.println("What do you want to do, user? Let's work or have a fun?");
        System.out.println("My system " + os + " is ready");
    }

    void printInfo() {
        System.out.println("Info about smartphone: ");
        System.out.println(name);
        System.out.println(serialNumber);
        System.out.println(getSimCount());
        System.out.println(getDisplay());
        System.out.println(getOs());
        System.out.println("---");
    }

    @Override
    public void powerOn() {
        super.powerOn();
        System.out.println("Hello, dear friend! I missed you.");
        System.out.println("---");
    }

    // option 1 - без varargs
//    @Override
//    public String powerOff() {
//        return "Bye, dear friend! I'll be waiting for you.";
//
//    }

    // option 2 - з varargs
    @Override
    public void powerOff() {
        System.out.println("Bye, dear friend! I'll be waiting for you.");

    }

    @Override
    void call() {
        System.out.println("How do you want to call? Throughout SIM card or by social networks?");
        System.out.println("You are calling the number: " + number);
        System.out.println("---");

    }

}
